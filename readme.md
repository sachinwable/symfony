<h3>AddressBook</h3>
<br />
The address book contains the following data:<br />
Firstname<br />
Lastname<br />
Street and number<br />
Zip<br />
City<br />
Country<br />
Phonenumber<br />
Birthday<br />
Email address<br />
Picture (optional)<br />
<br />
<h3>Requirements</h3><br />
Symfony 5.4<br />
Doctrine with SQLite<br />
Twig<br />
PHP 8.1<br />
<br />
<h3>Database</h3><br />
Run these commands to build SQLite scheme object in var to store data.<br />
- php bin/console make:migration<br />
- php bin/console doctrine:migrations:migrate
<br />
Command to start server and run hte project<br />
symfony server:start<br /><br />

<h3>Endpoints</h3><br />
/addresses = To show list of all addresses. This also has the form on the top to create<br />
/addresses/{id} = To edit the address<br />
/delete/{id} => To delete the address<br />