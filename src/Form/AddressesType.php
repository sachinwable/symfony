<?php

namespace App\Form;

use App\Entity\Address;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class,[
                'label' => 'Fist Name',
                'row_attr' => [
                    'class' => 'input-group',
                ],
                'required' => true,
            ])
            ->add('lastname', TextType::class,[
                'label' => 'Last Name',
                'row_attr' => [
                    'class' => 'input-group',
                ],
                'required' => true,
            ])
            ->add('city', TextType::class,[
                'label' => 'City',
                'row_attr' => [
                    'class' => 'input-group',
                ],
                'required' => true,
            ])
            ->add('country', TextType::class,[
                'label' => 'Country',
                'row_attr' => [
                    'class' => 'input-group',
                ],
                'required' => true,
            ])
            ->add('street', TextType::class,[
                'label' => 'Street and number',
                'row_attr' => [
                    'class' => 'input-group',
                ],
                'required' => true,
            ])
            ->add('zip', TextType::class,[
                'label' => 'Zip',
                'row_attr' => [
                    'class' => 'input-group',
                ],
                'required' => true,
            ])
            ->add('phone', TextType::class,[
                'label' => 'Phone Number',
                'row_attr' => [
                    'class' => 'input-group',
                ],
                'required' => true,
            ])
            ->add('email', TextType::class,[
                'label' => 'Email',
                'row_attr' => [
                    'class' => 'input-group',
                ],
                'required' => true,
            ])
            ->add('dob', DateType::class,[
                'label' => 'Date of Birth',
                'row_attr' => [
                    'class' => 'input-group',
                ],
                'widget' => 'single_text',
                // this is actually the default format for single_text
                'format' => 'yyyy-MM-dd',
                'required' => true,
            ])
            ->add('photo', FileType::class,[
                'label' => 'Picture',
                'row_attr' => [
                    'class' => 'input-group',
                ],
                'data_class' => null,
                'required' => false,
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Address::class,
        ]);
    }
}
