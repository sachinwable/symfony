<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Address;
use App\Form\AddressesType;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\AddressRepository;
use App\Service\FileUploader;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;

class AddressController extends AbstractController
{
    /**
     * @Route("/new", name="add_new_address")
     */
    public function new(Request $request, ManagerRegistry $doctrine, FileUploader $fileUploader): Response
    {
        $address = new Address();
        $form = $this->createForm(AddressesType::class, $address);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $address = $form->getData();
            $entityManager = $doctrine->getManager();

            $newAddress = new Address();
            $newAddress->setName($address->getName());
            $newAddress->setLastname($address->getLastname());
            $newAddress->setStreet($address->getStreet());
            $newAddress->setZip($address->getZip());
            $newAddress->setCity($address->getCity());
            $newAddress->setCountry($address->getCountry());
            $newAddress->setPhone($address->getPhone());
            $newAddress->setDob($address->getDob());
            $newAddress->setEmail($address->getEmail());
            $photo = $form->get('photo')->getData();

            if ($photo) {
                $photoFileName = $fileUploader->upload($photo);
                $newAddress->setPhoto($photoFileName);
            }

            $entityManager->persist($newAddress);
            $entityManager->flush();

            return $this->redirectToRoute('show_all_addresses');
        }

        //Show create form on this page incase /new url is accessed
        return $this->renderForm('address/new.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete_address")
     */
    public function deleteAddress(int $id, AddressRepository $addressRepository, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();

        $address = $addressRepository->find($id);

        if (!$address) {
            throw $this->createNotFoundException(
                'No address found for id '.$id
            );
        }

        $entityManager->remove($address);

        $entityManager->flush();

       return $this->redirectToRoute('show_all_addresses');
    }

    /**
     * @Route("/addresses/{id}", name="show_all_addresses")
     */
    public function showAllAddresses(Request $request, AddressRepository $addressRepository, ManagerRegistry $doctrine, FileUploader $fileUploader, $id=0)
    {
        if (!empty ($id)) {
            $address = $addressRepository->find($id);

            if ($address->getPhoto()) {
                $address->setPhoto(
                    new File($this->getParameter('photo_directory').'/'.$address->getPhoto())
                );
            }

            $form = $this->createForm(AddressesType::class, $address);

            $address->setPhoto(basename($address->getPhoto()));
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                
                $photo = $form->get('photo')->getData();

                if ($photo) {
                    $photoFileName = $fileUploader->upload($photo);
                    $address->setPhoto($photoFileName);
                }

                $entityManager = $doctrine->getManager();
               
                $entityManager->persist($address);
                $entityManager->flush();

                return $this->redirectToRoute('show_all_addresses');
            }
        } 
        
        if (empty ($id)){
            $address = new Address();
            $form = $this->createForm(AddressesType::class, $address, [
            'action' => $this->generateUrl('add_new_address'),
            'method' => 'POST',
        ]);
        }

        $addresses = $addressRepository->findAll();

        return $this->render(
            'address/index.html.twig',
            array('addresses' => $addresses, 'form' => $form->createView())
        );
    }
}
