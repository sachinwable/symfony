<?php

namespace ContainerXFUijlE;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/src/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder09b33 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer4b21b = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesd53ba = [
        
    ];

    public function getConnection()
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'getConnection', array(), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'getMetadataFactory', array(), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'getExpressionBuilder', array(), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'beginTransaction', array(), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'getCache', array(), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->getCache();
    }

    public function transactional($func)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'transactional', array('func' => $func), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'wrapInTransaction', array('func' => $func), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'commit', array(), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->commit();
    }

    public function rollback()
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'rollback', array(), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'getClassMetadata', array('className' => $className), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'createQuery', array('dql' => $dql), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'createNamedQuery', array('name' => $name), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'createQueryBuilder', array(), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'flush', array('entity' => $entity), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'clear', array('entityName' => $entityName), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->clear($entityName);
    }

    public function close()
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'close', array(), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->close();
    }

    public function persist($entity)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'persist', array('entity' => $entity), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'remove', array('entity' => $entity), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'refresh', array('entity' => $entity), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'detach', array('entity' => $entity), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'merge', array('entity' => $entity), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'getRepository', array('entityName' => $entityName), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'contains', array('entity' => $entity), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'getEventManager', array(), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'getConfiguration', array(), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'isOpen', array(), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'getUnitOfWork', array(), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'getProxyFactory', array(), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'initializeObject', array('obj' => $obj), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'getFilters', array(), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'isFiltersStateClean', array(), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'hasFilters', array(), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return $this->valueHolder09b33->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer4b21b = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder09b33) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder09b33 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder09b33->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, '__get', ['name' => $name], $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        if (isset(self::$publicPropertiesd53ba[$name])) {
            return $this->valueHolder09b33->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder09b33;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder09b33;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder09b33;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder09b33;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, '__isset', array('name' => $name), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder09b33;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder09b33;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, '__unset', array('name' => $name), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder09b33;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder09b33;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, '__clone', array(), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        $this->valueHolder09b33 = clone $this->valueHolder09b33;
    }

    public function __sleep()
    {
        $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, '__sleep', array(), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;

        return array('valueHolder09b33');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer4b21b = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer4b21b;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer4b21b && ($this->initializer4b21b->__invoke($valueHolder09b33, $this, 'initializeProxy', array(), $this->initializer4b21b) || 1) && $this->valueHolder09b33 = $valueHolder09b33;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder09b33;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder09b33;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
