<?php

namespace ContainerO5i1bgF;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_ULKJTt8Service extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.ULKJTt8' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.ULKJTt8'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'addressRepository' => ['privates', 'App\\Repository\\AddressRepository', 'getAddressRepositoryService', true],
            'doctrine' => ['services', 'doctrine', 'getDoctrineService', false],
        ], [
            'addressRepository' => 'App\\Repository\\AddressRepository',
            'doctrine' => '?',
        ]);
    }
}
