<?php

namespace ContainerFRqiHN8;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/src/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder74299 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializerb58c0 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiese5324 = [
        
    ];

    public function getConnection()
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'getConnection', array(), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'getMetadataFactory', array(), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'getExpressionBuilder', array(), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'beginTransaction', array(), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->beginTransaction();
    }

    public function getCache()
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'getCache', array(), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->getCache();
    }

    public function transactional($func)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'transactional', array('func' => $func), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'wrapInTransaction', array('func' => $func), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'commit', array(), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->commit();
    }

    public function rollback()
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'rollback', array(), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'getClassMetadata', array('className' => $className), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'createQuery', array('dql' => $dql), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'createNamedQuery', array('name' => $name), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'createQueryBuilder', array(), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'flush', array('entity' => $entity), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'clear', array('entityName' => $entityName), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->clear($entityName);
    }

    public function close()
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'close', array(), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->close();
    }

    public function persist($entity)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'persist', array('entity' => $entity), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'remove', array('entity' => $entity), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'refresh', array('entity' => $entity), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'detach', array('entity' => $entity), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'merge', array('entity' => $entity), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'getRepository', array('entityName' => $entityName), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'contains', array('entity' => $entity), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'getEventManager', array(), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'getConfiguration', array(), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'isOpen', array(), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'getUnitOfWork', array(), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'getProxyFactory', array(), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'initializeObject', array('obj' => $obj), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'getFilters', array(), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'isFiltersStateClean', array(), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'hasFilters', array(), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return $this->valueHolder74299->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializerb58c0 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder74299) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder74299 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder74299->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, '__get', ['name' => $name], $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        if (isset(self::$publicPropertiese5324[$name])) {
            return $this->valueHolder74299->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder74299;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder74299;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder74299;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder74299;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, '__isset', array('name' => $name), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder74299;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder74299;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, '__unset', array('name' => $name), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder74299;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder74299;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, '__clone', array(), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        $this->valueHolder74299 = clone $this->valueHolder74299;
    }

    public function __sleep()
    {
        $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, '__sleep', array(), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;

        return array('valueHolder74299');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerb58c0 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerb58c0;
    }

    public function initializeProxy() : bool
    {
        return $this->initializerb58c0 && ($this->initializerb58c0->__invoke($valueHolder74299, $this, 'initializeProxy', array(), $this->initializerb58c0) || 1) && $this->valueHolder74299 = $valueHolder74299;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder74299;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder74299;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
