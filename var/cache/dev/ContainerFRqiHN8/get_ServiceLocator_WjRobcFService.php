<?php

namespace ContainerFRqiHN8;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_WjRobcFService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.WjRobcF' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.WjRobcF'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'App\\Controller\\AddressController::deleteAddress' => ['privates', '.service_locator.ULKJTt8', 'get_ServiceLocator_ULKJTt8Service', true],
            'App\\Controller\\AddressController::new' => ['privates', '.service_locator.SgmGmTY', 'get_ServiceLocator_SgmGmTYService', true],
            'App\\Controller\\AddressController::showAllAddresses' => ['privates', '.service_locator.o2zJXoe', 'get_ServiceLocator_O2zJXoeService', true],
            'App\\Kernel::loadRoutes' => ['privates', '.service_locator.xUrKPVU', 'get_ServiceLocator_XUrKPVUService', true],
            'App\\Kernel::registerContainerConfiguration' => ['privates', '.service_locator.xUrKPVU', 'get_ServiceLocator_XUrKPVUService', true],
            'App\\Kernel::terminate' => ['privates', '.service_locator.bRdave9', 'get_ServiceLocator_BRdave9Service', true],
            'kernel::loadRoutes' => ['privates', '.service_locator.xUrKPVU', 'get_ServiceLocator_XUrKPVUService', true],
            'kernel::registerContainerConfiguration' => ['privates', '.service_locator.xUrKPVU', 'get_ServiceLocator_XUrKPVUService', true],
            'kernel::terminate' => ['privates', '.service_locator.bRdave9', 'get_ServiceLocator_BRdave9Service', true],
            'App\\Controller\\AddressController:deleteAddress' => ['privates', '.service_locator.ULKJTt8', 'get_ServiceLocator_ULKJTt8Service', true],
            'App\\Controller\\AddressController:new' => ['privates', '.service_locator.SgmGmTY', 'get_ServiceLocator_SgmGmTYService', true],
            'App\\Controller\\AddressController:showAllAddresses' => ['privates', '.service_locator.o2zJXoe', 'get_ServiceLocator_O2zJXoeService', true],
            'kernel:loadRoutes' => ['privates', '.service_locator.xUrKPVU', 'get_ServiceLocator_XUrKPVUService', true],
            'kernel:registerContainerConfiguration' => ['privates', '.service_locator.xUrKPVU', 'get_ServiceLocator_XUrKPVUService', true],
            'kernel:terminate' => ['privates', '.service_locator.bRdave9', 'get_ServiceLocator_BRdave9Service', true],
        ], [
            'App\\Controller\\AddressController::deleteAddress' => '?',
            'App\\Controller\\AddressController::new' => '?',
            'App\\Controller\\AddressController::showAllAddresses' => '?',
            'App\\Kernel::loadRoutes' => '?',
            'App\\Kernel::registerContainerConfiguration' => '?',
            'App\\Kernel::terminate' => '?',
            'kernel::loadRoutes' => '?',
            'kernel::registerContainerConfiguration' => '?',
            'kernel::terminate' => '?',
            'App\\Controller\\AddressController:deleteAddress' => '?',
            'App\\Controller\\AddressController:new' => '?',
            'App\\Controller\\AddressController:showAllAddresses' => '?',
            'kernel:loadRoutes' => '?',
            'kernel:registerContainerConfiguration' => '?',
            'kernel:terminate' => '?',
        ]);
    }
}
