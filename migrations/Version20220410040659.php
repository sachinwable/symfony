<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220410040659 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE address ADD COLUMN photo VARCHAR(100) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__address AS SELECT id, name, city, country, phone, zip, street, lastname, email, dob FROM address');
        $this->addSql('DROP TABLE address');
        $this->addSql('CREATE TABLE address (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(50) DEFAULT NULL, city VARCHAR(100) DEFAULT NULL, country VARCHAR(100) DEFAULT NULL, phone VARCHAR(15) DEFAULT NULL, zip VARCHAR(8) DEFAULT NULL, street VARCHAR(255) DEFAULT NULL, lastname VARCHAR(25) DEFAULT NULL, email VARCHAR(50) DEFAULT NULL, dob DATE DEFAULT NULL)');
        $this->addSql('INSERT INTO address (id, name, city, country, phone, zip, street, lastname, email, dob) SELECT id, name, city, country, phone, zip, street, lastname, email, dob FROM __temp__address');
        $this->addSql('DROP TABLE __temp__address');
    }
}
